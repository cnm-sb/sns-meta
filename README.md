# 前后端分离论坛系统

## 程序介绍
- 轻论坛系统，目前只完善了基本功能，蜗速迭代中...

- 前端vue，后端golang

- 演示站：[http://sns.dh1.cc](http://sns.dh1.cc) (PC端UI未适配，请使用移动端预览)


## 写在前面

-  此教程异常详细，因此也相对繁琐，建站经验丰富的可按需略过，建站经验不足的请务必一步一步按照步骤操作，完全按照图文教程操作可避免80%的坑

-  程序目前还在开发阶段，不足之处请向我们提出建议，我们会认真对待，一一回复

-  程序所涉及的任何内容，包括但不限于文字，图片，图标，代码等等任何侵犯您权利的内容，请联系我们妥善处理！

-  企鹅交流群：826866498，大家加入群聊一起玩耍

    -  不想扫码？[点我加入](http://qm.qq.com/cgi-bin/qm/qr?_wv=1027&k=93nBg5lf-u3pR4bfSmMp4iiD4BjaxzSq&authKey=xuN0agwvBAckmy56wDcXr4N44HdTE4Qsdb5mOn8FDJzwBcpeaKf8LuedgQdBqd2X&noverify=0&group_code=826866498)
    -  <img src="other-images/826866498.jpg" alt="Q群826866498" width="300">


## 事先准备
- 一台服务器，运行环境如下
    - 操作系统：Linux Centos7.6
    - Web服务器：Nginx 1.22.1
    - 数据库：Mysql 5.7.40
    - 数据库：Redis 7.0.11
    - 其他版本环境请自行测试

- 一个域名，域名可以先解析三条记录，方便后面使用，不会解析的先跳过，下面有解析教程
    - 用户前台：例如 sns.cnm.sb
    - 管理员后台：例如 admin.cnm.sb
    - 后端API：例如 api.cnm.sb

- 程序安装包在群里面，请进QQ群获取，遇到疑问请@管理员
    - 群文件 笋丁轻论坛.zip


## 教程开始

### 🍓首先拥有一台自己的服务器
![拥有一台服务器](deployment-tutorial/1%E6%9C%8D%E5%8A%A1%E5%99%A8.png)


### 🥝安装Linux操作系统
![安装Linux操作系统](deployment-tutorial/2Linux%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F.png)
![安装Linux操作系统](deployment-tutorial/3%E5%AE%89%E8%A3%85%E6%93%8D%E4%BD%9C%E7%B3%BB%E7%BB%9F%E8%AE%BE%E7%BD%AE%E5%AF%86%E7%A0%81.png)


### 🍒服务器端口放行
![服务器端口放行](deployment-tutorial/%E5%AE%89%E5%85%A8%E7%BB%84%E6%94%BE%E8%A1%8C%E7%AB%AF%E5%8F%A3.png)


### 🍑安装宝塔面板
![安装宝塔面板](deployment-tutorial/4.png)
![安装宝塔面板](deployment-tutorial/5%E9%80%89%E6%8B%A9%E5%AE%89%E8%A3%85%E9%9D%A2%E6%9D%BF%E8%BF%98%E6%98%AF%E9%9D%A2%E6%9D%BF%E5%8A%A0%E7%8E%AF%E5%A2%83.png)
![安装宝塔面板](deployment-tutorial/6%E5%AE%89%E8%A3%85%E5%AE%8C%E6%88%90.png)


### 🍐登录宝塔面板账号，没有账号的请前往官网注册一个
![宝塔面板账号](deployment-tutorial/7%E7%99%BB%E5%BD%95%E8%B4%A6%E5%8F%B7.png)


### 🍏安装LNMP套件
![安装LNMP套件](deployment-tutorial/8%E5%AE%89%E8%A3%85%E7%8E%AF%E5%A2%83.png)


### 🍎安装Redis并设置密码
![安装Redis并设置密码](deployment-tutorial/%E5%AE%89%E8%A3%85redis.png)
![安装Redis并设置密码](deployment-tutorial/Redis%E8%AE%BE%E7%BD%AE%E5%AF%86%E7%A0%81.png)
![安装Redis并设置密码](deployment-tutorial/Redis%E9%87%8D%E8%BD%BD%E9%85%8D%E7%BD%AE.png)


### 🥭域名解析
![域名解析](deployment-tutorial/9%E5%9F%9F%E5%90%8D%E8%A7%A3%E6%9E%90.png)
![域名解析](deployment-tutorial/9-2%E5%9F%9F%E5%90%8D%E8%A7%A3%E6%9E%90.png)


### 🦀添加数据库
![添加数据库](deployment-tutorial/%E6%B7%BB%E5%8A%A0%E6%95%B0%E6%8D%AE%E5%BA%93.png)


### 🍍部署后端api
- # 此版本使用的端口是 6688
- # 此版本使用的端口是 6688
- # 此版本使用的端口是 6688
- # 此版本使用的端口是 6688
- # 此版本使用的端口是 6688

![部署golang后端](deployment-tutorial/10%E6%96%B0%E5%BB%BA%E6%96%87%E4%BB%B6%E5%A4%B9.png)
![部署golang后端](deployment-tutorial/11%E4%B8%8A%E4%BC%A0%E6%96%87%E4%BB%B6.png)
![部署golang后端](deployment-tutorial/12%E6%B7%BB%E5%8A%A0Go%E9%A1%B9%E7%9B%AE.png)

### 🍌提交部署之后，查看程序日志
![查看程序日志](deployment-tutorial/13%E6%9F%A5%E7%9C%8B%E7%A8%8B%E5%BA%8F%E6%97%A5%E5%BF%97.png)


### 🍋部署前端管理员后台
| 默认生成 | 用户名   | 密码     | 备注           |
|------|-------|--------|--------------|
| 用户   | user  | 123456 | 测试之后建议封禁此账号  |
| 管理员  | admin | 123456 | 登录成功之后务必修改密码 |

    # nginx反向代理代码，用户前台和管理员后台均使用这些代码
    location / {
        index index.html;
        try_files $uri $uri/ /index.html;
    }
    
    location ^~ /sd {
    	proxy_pass http://更换为你的后端域名，注意https和http，开启强制https必须填https;
    	proxy_set_header X-Real-IP $remote_addr;
        proxy_set_header REMOTE-HOST $remote_addr;
        proxy_set_header X-Forwarded-For $proxy_add_x_forwarded_for;
    }

![部署前端管理员后台](deployment-tutorial/admin/201%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0%E6%B7%BB%E5%8A%A0%E7%BD%91%E7%AB%99.png)
![部署前端管理员后台](deployment-tutorial/admin/202%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0%E4%B8%8A%E4%BC%A0%E6%BA%90%E7%A0%81%E8%A7%A3%E5%8E%8B.png)
![部署前端管理员后台](deployment-tutorial/admin/203%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0nginx%E5%8F%8D%E5%90%91%E4%BB%A3%E7%90%86.png)
![部署前端管理员后台](deployment-tutorial/admin/204%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0%E7%9A%84install%E9%A1%B5%E9%9D%A2.png)
![部署前端管理员后台](deployment-tutorial/admin/205%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0install%E9%85%8D%E7%BD%AE%E6%95%B0%E6%8D%AE%E5%BA%93.png)
![部署前端管理员后台](deployment-tutorial/admin/206%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0install%E9%A1%B5%E9%9D%A2%E9%85%8D%E7%BD%AEredis.png)
![部署前端管理员后台](deployment-tutorial/admin/207%E7%AE%A1%E7%90%86%E5%91%98%E5%90%8E%E5%8F%B0install%E5%AE%89%E8%A3%85%E5%AE%8C%E6%88%90%E9%A1%B5%E9%9D%A2.png)


### 🍊部署前端用户前台
- nginx配置和管理员后台的完全一样

![部署用户前台](deployment-tutorial/%E9%83%A8%E7%BD%B2%E5%89%8D%E7%AB%AF%E7%94%A8%E6%88%B7%E5%89%8D%E5%8F%B0.png)
![部署用户前台](deployment-tutorial/%E5%89%8D%E7%AB%AF%E7%94%A8%E6%88%B7%E5%89%8D%E5%8F%B0%E9%85%8D%E7%BD%AEnginx%E5%8F%8D%E5%90%91%E4%BB%A3%E7%90%86.png)


### 🍉自此程序部署结束，请访问用户前台域名查看，感谢您的支持！

版本迭代

    - 后端：直接替换后端的main文件，storage文件夹和config.yaml文件夹不用管，之后前往go项目重启即可

    - 前端：前端包括管理员后台和用户前台，直接删除index.html文件和assets文件夹，重新上传即可，nginx已经配置无需更改

## 🍈特别鸣谢

本项目使用了以下开源组件，并在此表示感谢：

- [Vue 3](https://github.com/vuejs/vue-next)
- [Vite](https://github.com/vitejs/vite)
- [Element Plus](https://github.com/element-plus/element-plus)
- [Pinia](https://github.com/vuejs/pinia)
- [Redis](https://github.com/redis/redis)
- [MySQL](https://github.com/mysql/mysql-server)

- [Golang](https://github.com/golang/go)
- [Gin](https://github.com/gin-gonic/gin)
- 等等，待完善

感谢以上组件的作者为我们提供优秀的开源软件！

## 🔑 License
MIT

## 🍇更多玩法
- QQ群聊内机器人快捷发帖等 [点击前往](QqRobot.md)
- QQ频道内机器人快捷发帖等 [点击前往](QqChannelRobot.md)
- 微信群聊内机器人快捷发帖等（编写中）
- 微信公众号机器人快捷发帖等（编写中）

## 💰赞助
- 赞助二维码（赞助之后请联系我们，将您添加到名单中）

    <img src="other-images/%E6%89%93%E8%B5%8F.png" alt="Q群826866498" width="375">

- 名单
    
    | ID | 昵称 | 金额 | 留言 |
    |----|----|----|----|
    | 1  | 半成熟  | ¥130  | 每日一碗毒鸡汤 [http://20.re](http://20.re)  |
    | 2  | 噗噗子  | ¥40  | 大肥羊学校干饭大王  |
    | 2  | 紫夜  | ¥20  | 好人一生平安  |
    | 3  | 小丑同学  | ¥20  | 拼搏进取 和谐创新  |
    | 4  | 吊哥  | ¥8  | 发财的路上，一直发  |
    | 5  | PONY  | ¥1  | 念念不忘必有回响  |
    

Copyright (c) 2023 sunding
