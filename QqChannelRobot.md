## QQ频道机器人快捷发帖部署教程

### 官方频道
[点击链接加入QQ频道体验：https://pd.qq.com/s/gfocetvkv](https://pd.qq.com/s/gfocetvkv)

### 先在手机QQ里面创建一个频道
![创建频道](other-images/qq-channel-robot/600%E5%88%9B%E5%BB%BA%E9%A2%91%E9%81%93.png)


### 注册QQ开放平台账号
![注册](other-images/qq-channel-robot/601%E6%B3%A8%E5%86%8C%E8%B4%A6%E5%8F%B7.png)
![绑定管理员](other-images/qq-channel-robot/602%E7%BB%91%E5%AE%9A%E7%AE%A1%E7%90%86%E5%91%98.png)
![注册成功](other-images/qq-channel-robot/603%E6%B3%A8%E5%86%8C%E6%88%90%E5%8A%9F.png)


### 创建机器人
![创建机器人](other-images/qq-channel-robot/604%E5%88%9B%E5%BB%BA%E6%9C%BA%E5%99%A8%E4%BA%BA.png)
![提交审核](other-images/qq-channel-robot/605%E6%8F%90%E4%BA%A4%E5%AE%A1%E6%A0%B8.png)

### 获取机器人参数
![获取机器人参数](other-images/qq-channel-robot/606%E5%BC%80%E5%8F%91%E5%8F%82%E6%95%B0.png)


### 配置消息url(如果不需要可以不用配置)
![配置消息url](other-images/qq-channel-robot/607%E9%85%8D%E7%BD%AE%E6%B6%88%E6%81%AFurl.png)


### 添加机器人到频道中
![添加机器人到频道中](other-images/qq-channel-robot/608%E6%B7%BB%E5%8A%A0%E6%9C%BA%E5%99%A8%E4%BA%BA%E5%88%B0%E9%A2%91%E9%81%93%E4%B8%AD.png)


### 管理员后台填写参数，启动频道机器人

登录管理员后台，填写参数启动即可，非常简单的，没时间截图了，不截了