## QQ机器人群聊内快捷发帖到网站

### 事先准备
- 请先搭建好网站再进行部署QQ群聊机器人

- 基于go-cqhttp[点我前往](https://docs.go-cqhttp.org/)，有问题请前往机器人官网查询解决


### 下载程序
![输入图片说明](other-images/qq-robot/400%E7%A8%8B%E5%BA%8F.png)


### 启动签名程序
![启动签名程序](other-images/qq-robot/401%E5%90%AF%E5%8A%A8.png)


### 修改qq机器人的配置文件
![修改qq机器人的配置文件](other-images/qq-robot/402qq-robot%E4%BF%AE%E6%94%B9yaml%E6%96%87%E4%BB%B6.png)


### 执行登录
![执行登录](other-images/qq-robot/403qq-robot%E7%99%BB%E5%BD%95.png)


### 校验验证码（注意：先F12打开控制台，再滑块验证，再Ctrl + F进行搜索）
![校验验证码](other-images/qq-robot/404%E5%A4%8D%E5%88%B6%E9%AA%8C%E8%AF%81%E7%A0%81%E9%93%BE%E6%8E%A5.png)
![校验验证码](other-images/qq-robot/405%E6%89%8B%E5%8A%A8%E6%8A%93%E5%8F%96%E9%AA%8C%E8%AF%81%E7%A0%81.png)
![校验验证码](other-images/qq-robot/406%E7%B2%98%E8%B4%B4ticket%E6%8F%90%E4%BA%A4.png)


### 校验设备锁
![校验设备锁](other-images/qq-robot/407%E8%BE%93%E5%85%A5%E8%AE%BE%E5%A4%87%E9%94%81%E7%9F%AD%E4%BF%A1%E9%AA%8C%E8%AF%81%E7%A0%81.png)


### 登录成功
![登录成功](other-images/qq-robot/408QQ%E7%99%BB%E5%BD%95%E6%88%90%E5%8A%9F.png)


### 配置消息处理程序的参数并启动
![配置消息处理程序的参数](other-images/qq-robot/409%E9%85%8D%E7%BD%AE%E6%B6%88%E6%81%AF%E5%A4%84%E7%90%86%E5%8F%82%E6%95%B0.png)
![配置消息处理程序的参数](other-images/qq-robot/410%E6%B6%88%E6%81%AF%E5%A4%84%E7%90%86%E5%90%AF%E5%8A%A8%E6%88%90%E5%8A%9F%E9%A1%B5%E9%9D%A2.png)


### 最后全部完成的页面
![最后全部完成的页面](other-images/qq-robot/411%E6%9C%80%E5%90%8E%E5%AE%8C%E6%88%90%E5%9B%BE.png)


### 上传到挂机宝
![上传到挂机宝](other-images/qq-robot/412sessionToken%E6%96%87%E4%BB%B6.png)

### 完结撒花